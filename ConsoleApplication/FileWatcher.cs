﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using CustomConfiguration;
using NLog;
using File = System.IO.File;

namespace ConsoleApplication
{
	public class FileWatcher
	{
		private static readonly Logger logger = LogManager.GetCurrentClassLogger();

		private Dictionary<string, List<string>> currentFilesInFolders;

		private static readonly string defaulFolderKey = "defaultFolder";
		private Dictionary<string, List<string>> originalFilesInFolders;

		private readonly List<Rule> rules;

		public FileWatcher(string culture)
		{
			originalFilesInFolders = new Dictionary<string, List<string>>();
			rules = new List<Rule>();
			
			Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(culture); ;

			GetValuesFromFileConfiguration();
		}

		public void StartWatch()
		{
			GetOriginalFilesInFolders();
			TimerCallback callback = CheckChangesInFolders;
			var timer = new Timer(callback, null, 0, 1000);
		}

		private void GetOriginalFilesInFolders()
		{
			originalFilesInFolders.Keys.ToList().ForEach(key => originalFilesInFolders[key] = Directory.GetFiles(key).ToList());
		}

		private void CheckChangesInFolders(object obj)
		{
			currentFilesInFolders = new Dictionary<string, List<string>>();
			originalFilesInFolders.Keys.ToList()
				.ForEach(key => currentFilesInFolders.Add(key, Directory.GetFiles(key).ToList()));

			foreach (var key in currentFilesInFolders.Keys)
			{
				foreach (var file in currentFilesInFolders[key])
				{
					if (!originalFilesInFolders[key].Contains(file))
					{
						logger.Info(CultureInfo.CurrentCulture, Messages.AddMessage, $"'{file}'", key);
						MoveFile(file);
					}
				}
			}

			originalFilesInFolders = new Dictionary<string, List<string>>();
			currentFilesInFolders.Keys.ToList()
				.ForEach(key => originalFilesInFolders.Add(key, Directory.GetFiles(key).ToList()));
		}

		private void GetValuesFromFileConfiguration()
		{
			var section = (CustomSection) ConfigurationManager.GetSection("WatchFolders");
			if (section != null && section.WatchFolders.Count != 0)
			{
				for (var i = 0; i < section.WatchFolders.Count; i++)
				{
					if (Directory.Exists((string) Resources.ResourceManager.GetObject(section.WatchFolders[i].Path)))
					{
						originalFilesInFolders.Add((string) Resources.ResourceManager.GetObject(section.WatchFolders[i].Path), null);
					}
					else
					{
						logger.Info(CultureInfo.CurrentCulture, Messages.NotExistDictionaryMessage, $"'{Resources.ResourceManager.GetObject(section.WatchFolders[i].Path)}'");
					}
				}
			}

			var patternSection = (FileSection) ConfigurationManager.GetSection("RulesForFile");
			if (patternSection != null && patternSection.RulesForFile.Count != 0)
			{
				for (var i = 0; i < patternSection.RulesForFile.Count; i++)
				{
					if (Directory.Exists(Resources.ResourceManager.GetObject(patternSection.RulesForFile[i].Destination)?.ToString()))
					{
						rules.Add(new Rule
						{
							Pattern = Resources.ResourceManager.GetObject(patternSection.RulesForFile[i].Pattern)?.ToString(),
							FolderDestination = Resources.ResourceManager.GetObject(patternSection.RulesForFile[i].Destination)?.ToString(),
							IsAddSequenceNumber = patternSection.RulesForFile[i].IsAddSequenceNumber.Equals("true"),
							IsAddDateMove = patternSection.RulesForFile[i].IsAddDateMove.Equals("true")
						});
					}
					else
					{
						logger.Info(CultureInfo.CurrentCulture, Messages.NoneExistenFolder, $"'{Directory.Exists(Resources.ResourceManager.GetObject(patternSection.RulesForFile[i].Destination)?.ToString())}'");
					}
				}
			}
		}

		private void MoveFile(string pathToFile)
		{
			var file = new FileInfo(pathToFile);

			var fileMoved = false;
			foreach (var rule in rules)
			{
				if (Regex.IsMatch(file.Name, rule.Pattern, RegexOptions.IgnoreCase))
				{
					fileMoved = !fileMoved;
					logger.Info(CultureInfo.CurrentCulture, Messages.FileMathesMessage, $"'{file.Name}'", rule.Pattern);
					ChangeNameAndMoveFile(file, rule);
				}
			}

			if (!fileMoved)
			{
				logger.Info(CultureInfo.CurrentCulture, Messages.NotFoundMathces, file.Name);
				ChangeNameAndMoveFile(file,
					new Rule
					{
						FolderDestination = Resources.defaultFolder,
						IsAddDateMove = true,
						IsAddSequenceNumber = true
					});
			}
		}

		private void ChangeNameAndMoveFile(FileInfo file, Rule ruleMoving)
		{
			var newPath = string.Empty;
			if (ruleMoving.IsAddSequenceNumber)
			{
				for (var i = 1; i < int.MaxValue; i++)
				{
					newPath = $"{ruleMoving.FolderDestination}/{Path.GetFileNameWithoutExtension(file.Name)}{i}{file.Extension}";
					if (File.Exists(newPath))
					{
					}
					else
					{
						try
						{
							File.Move(file.FullName, newPath);
							logger.Info(CultureInfo.CurrentCulture, Messages.FileMoved, $"'{file.Name}'", ruleMoving.FolderDestination);
						}
						catch (IOException)
						{
							logger.Info(CultureInfo.CurrentCulture, Messages.CanNotAccess, $"'{file.Name}'");
						}

						break;
					}
				}
			}
			else
			{
				newPath = $"{ruleMoving.FolderDestination}/{file.Name}";
				if (File.Exists(newPath))
				{
					logger.Info(CultureInfo.CurrentCulture, Messages.CanNotMove, $"'{newPath}'");
				}
				else
				{
					try
					{
						File.Move(file.FullName, newPath);
						logger.Info(CultureInfo.CurrentCulture, Messages.FileMoved, $"'{file.Name}'", ruleMoving.FolderDestination);
					}
					catch (IOException)
					{
						logger.Info(CultureInfo.CurrentCulture, Messages.CanNotAccess, $"'{file.Name}'");
					}
				}
			}

			if (ruleMoving.IsAddDateMove)
			{
				var movedFile = new FileInfo(newPath);
				movedFile.LastWriteTime = DateTime.Now;
			}
		}
	}
}