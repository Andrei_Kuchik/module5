﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace ConsoleApplication
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var culture = ConfigurationManager.AppSettings["Culture"];

			if (string.IsNullOrEmpty(culture))
			{
				culture = "ru-Ru";
			}
			
			var watcher = new FileWatcher(culture);
			watcher.StartWatch();

			Console.CancelKeyPress += ClickHandler;
			Console.WriteLine(Messages.StartMessage);
			while (Console.Read() != 'q'){
			}
		}

		protected static void ClickHandler(object sender, ConsoleCancelEventArgs args)
		{
			Process.GetCurrentProcess().Kill();
		}
	}
}