﻿namespace ConsoleApplication
{
	public class Rule
	{
		public string Pattern { get; set; }
		public string FolderDestination { get; set; }
		public bool IsAddSequenceNumber { get; set; }
		public bool IsAddDateMove { get; set; }
	}
}