﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleApplication {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Messages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Messages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ConsoleApplication.Messages", typeof(Messages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Added new file {0} in {1}.
        /// </summary>
        internal static string AddMessage {
            get {
                return ResourceManager.GetString("AddMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The process cannot access {0} because it is being used by another process..
        /// </summary>
        internal static string CanNotAccess {
            get {
                return ResourceManager.GetString("CanNotAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot move file because it already exist {0}.
        /// </summary>
        internal static string CanNotMove {
            get {
                return ResourceManager.GetString("CanNotMove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File {0} matches regular expression {1}.
        /// </summary>
        internal static string FileMathesMessage {
            get {
                return ResourceManager.GetString("FileMathesMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File {0} moved to {1}.
        /// </summary>
        internal static string FileMoved {
            get {
                return ResourceManager.GetString("FileMoved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nonexistent folder in the list of rules: {0}.
        /// </summary>
        internal static string NoneExistenFolder {
            get {
                return ResourceManager.GetString("NoneExistenFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Directory not exist {0}.
        /// </summary>
        internal static string NotExistDictionaryMessage {
            get {
                return ResourceManager.GetString("NotExistDictionaryMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not found matches regular expression for this file {0}.
        /// </summary>
        internal static string NotFoundMathces {
            get {
                return ResourceManager.GetString("NotFoundMathces", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter &apos;q&apos; or click CTRL+C or CTRL+BREAK for finish to execute application.
        /// </summary>
        internal static string StartMessage {
            get {
                return ResourceManager.GetString("StartMessage", resourceCulture);
            }
        }
    }
}
