﻿using System.Configuration;

namespace CustomConfiguration
{
	public class FilesCollection : ConfigurationElementCollection
	{
		public FilesCollection()
		{
			AddElementName = "File";
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new File();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return (element as File).Destination;
		}

		public File this[int idx] => BaseGet(idx) as File;
	}
}