﻿using System.Configuration;

namespace CustomConfiguration
{
	public class FoldersCollection : ConfigurationElementCollection
	{
		public FoldersCollection()
		{
			AddElementName = "Folder";
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new Folder();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return (element as Folder).Path;
		}

		public Folder this[int idx] => BaseGet(idx) as Folder;
	}
}