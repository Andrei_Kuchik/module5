﻿using System.Configuration;

namespace CustomConfiguration
{
	public class FileSection : ConfigurationSection
	{
		public const string sectionName = "RulesForFile";

		[ConfigurationProperty("", IsDefaultCollection = true)]
		public FilesCollection RulesForFile => this[""] as FilesCollection;

	}
}