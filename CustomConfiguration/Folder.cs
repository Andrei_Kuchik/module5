﻿using System.Configuration;

namespace CustomConfiguration
{
	public class Folder : ConfigurationElement
	{
		private const string ATTRIBUTE_PATH = "path";

		[ConfigurationProperty(ATTRIBUTE_PATH, DefaultValue = "", IsKey = false, IsRequired = true)]
		public string Path
		{
			get => this[ATTRIBUTE_PATH].ToString();
			set => this[ATTRIBUTE_PATH] = value;
		}
	}
}