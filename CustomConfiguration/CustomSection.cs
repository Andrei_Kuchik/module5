﻿using System.Configuration;

namespace CustomConfiguration
{
	public class CustomSection : ConfigurationSection
	{
		public const string sectionName = "WatchFolders";

		[ConfigurationProperty("", IsDefaultCollection = true)]
		public FoldersCollection WatchFolders => this[""] as FoldersCollection;

	}
}