﻿using System.Configuration;

namespace CustomConfiguration
{
	public class File : ConfigurationElement
	{
		private const string ATTRIBUTE_PATTERN = "pattern";
		private const string ATTRIBUTE_DESTINATION = "folderDestination";
		private const string ATTRIBUTE_SEQUENCENUMBER = "isAddSequenceNumber";
		private const string ATTRIBUTE_DATEMOVE = "IsAddDateMove";

		[ConfigurationProperty(ATTRIBUTE_PATTERN, DefaultValue = "", IsKey = false, IsRequired = true)]
		public string Pattern
		{
			get => this[ATTRIBUTE_PATTERN].ToString();
			set => this[ATTRIBUTE_PATTERN] = value;
		}

		[ConfigurationProperty(ATTRIBUTE_DESTINATION, DefaultValue = "", IsKey = false, IsRequired = true)]
		public string Destination
		{
			get => this[ATTRIBUTE_DESTINATION].ToString();
			set => this[ATTRIBUTE_DESTINATION] = value;
		}

		[ConfigurationProperty(ATTRIBUTE_SEQUENCENUMBER, DefaultValue = "", IsKey = false, IsRequired = false)]
		public string IsAddSequenceNumber
		{
			get => this[ATTRIBUTE_SEQUENCENUMBER].ToString();
			set => this[ATTRIBUTE_SEQUENCENUMBER] = value;
		}

		[ConfigurationProperty(ATTRIBUTE_DATEMOVE, DefaultValue = "", IsKey = false, IsRequired = false)]
		public string IsAddDateMove
		{
			get => this[ATTRIBUTE_DATEMOVE].ToString();
			set => this[ATTRIBUTE_DATEMOVE] = value;
		}
	}
}